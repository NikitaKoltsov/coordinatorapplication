//
//  LoginCoordinator.swift
//  CoordinatorApplication
//
//  Created by Nikita Koltsov on 3/26/18.
//  Copyright © 2018 vironIT. All rights reserved.
//

import UIKit

final class LoginCoordinator: BaseCoordinator, LoginCoordinatorOutput {
    
    // MARK: - Properties
    
    private let router: RouterType
    private let dataService: DataService
    private let loginFactory: LoginModuleFactoryType
    var finishFlow: (() -> Void)?
    
    // MARK: - Lifecycle
    
    init(router: RouterType, dataService: DataService, loginFactory: LoginModuleFactoryType) {
        self.router = router
        self.dataService = dataService
        self.loginFactory = loginFactory
    }
    
    // MARK: - BaseCoordinator
    
    override func start() {
        showSignIn()
    }
    
    // MARK: - Navigation
    
    private func showSignIn() {
        let vc = loginFactory.makeSignInModule(apiService: dataService.apiService)
        vc.onSignUpAction = { [weak self] in
            self?.showSignUp()
        }
        vc.onSuccessSignIn = { [weak self] userModel in
            self?.finishAuthWith(userModel: userModel)
        }
        
        router.setRoot(vc)
    }
    
    private func showSignUp() {
        let vc = loginFactory.makeSignUpModule(apiService: dataService.apiService)
        vc.onSignInAction = { [weak self] in
            self?.router.popModule()
        }
        vc.onSuccessSignUp = { [weak self] userModel in
            self?.finishAuthWith(userModel: userModel)
        }
        
        router.push(vc)
    }
    
    private func finishAuthWith(userModel: UserModelType) {
        dataService.loginService.login(userModel)
        finishFlow?()
    }
    
}
