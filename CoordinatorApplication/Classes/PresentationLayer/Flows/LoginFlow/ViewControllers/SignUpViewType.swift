//
//  SignUpViewType.swift
//  CoordinatorApplication
//
//  Created by Nikita Koltsov on 3/21/18.
//  Copyright © 2018 vironIT. All rights reserved.
//

protocol SignUpViewType: Presentable {
    var onSuccessSignUp: ((UserModelType) -> Void)? { get set }
    var onSignInAction: (() -> Void)? { get set }
}
