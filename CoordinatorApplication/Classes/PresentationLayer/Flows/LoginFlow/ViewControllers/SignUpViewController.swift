//
//  SignUpViewController.swift
//  CoordinatorApplication
//
//  Created by Nikita Koltsov on 3/21/18.
//  Copyright © 2018 vironIT. All rights reserved.
//

import UIKit

final class SignUpViewController: UIViewController, SignUpViewType {
    
    // MARK: - Outlets
    
    @IBOutlet private weak var userNameTextField: UITextField!
    @IBOutlet private weak var passwordTextField: UITextField!
    @IBOutlet private weak var repeatPasswordTextField: UITextField!
    @IBOutlet private weak var emailTextField: UITextField!
    
    // MARK: - Properties
    
    var apiService: APIServiceType!
    
    // MARK: - Controller handlers
    var onSuccessSignUp: ((UserModelType) -> Void)?
    var onSignInAction: (() -> Void)?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }


    // MARK: - Actions
    
    @IBAction private func registerAction(_ sender: UIButton) {
        validateAndRegister()
    }
    
    @IBAction private func haveAnAccountAction(_ sender: UIButton) {
        onSignInAction?()
    }
    
    // MARK: - Convenience
    
    private func validateAndRegister() {
        guard let userName = userNameTextField.text, !userName.isEmpty else {
            showAlertWith(title: "Invalid input", message: "You haven't entered user name", completion: {
                self.userNameTextField.becomeFirstResponder()
            })
            return
        }
        
        guard let password = passwordTextField.text, !password.isEmpty else {
            showAlertWith(title: "Invalid input", message: "You haven't entered password", completion: {
                self.passwordTextField.becomeFirstResponder()
            })
            return
        }
        
        guard let repeatPassword = repeatPasswordTextField.text,
            repeatPassword == password else {
                showAlertWith(title: "Invalid input", message: "Passwords should match", completion: {
                    self.repeatPasswordTextField.becomeFirstResponder()
                })
                return
        }
        
        apiService.registerWith(userName, password: password, email: emailTextField.text) { [weak self] error, userModel in
            if let error = error {
                self?.showAlertWith(title: "Failure", message: error.localizedDescription)
            } else if let userModel = userModel {
                self?.onSuccessSignUp?(userModel)
            } else {
                self?.showAlertWith(title: "Failure", message: "Unknown error")
            }
        }
    }
}

extension SignUpViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField.tag {
        case userNameTextField.tag:
            passwordTextField.becomeFirstResponder()
        case passwordTextField.tag:
            repeatPasswordTextField.becomeFirstResponder()
        case repeatPasswordTextField.tag:
            emailTextField.becomeFirstResponder()
        case emailTextField.tag:
            emailTextField.resignFirstResponder()
        default:
            break
        }
        return false
    }
}
