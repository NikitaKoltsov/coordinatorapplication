//
//  SignInViewType.swift
//  CoordinatorApplication
//
//  Created by Nikita Koltsov on 3/20/18.
//  Copyright © 2018 vironIT. All rights reserved.
//

protocol SignInViewType: Presentable {
    var onSuccessSignIn: ((UserModelType) -> Void)? { get set }
    var onSignUpAction: (() -> Void)? { get set }
}
