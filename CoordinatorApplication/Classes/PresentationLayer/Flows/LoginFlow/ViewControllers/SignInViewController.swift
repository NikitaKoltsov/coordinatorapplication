//
//  LoginViewController.swift
//  CoordinatorApplication
//
//  Created by Nikita Koltsov on 3/16/18.
//  Copyright © 2018 vironIT. All rights reserved.
//

import UIKit

final class SignInViewController: UIViewController, SignInViewType {

    // MARK: - Outlets
    
    @IBOutlet private weak var userNameTextField: UITextField!
    @IBOutlet private weak var passwordTextField: UITextField!
    
    // MARK: - Properties
    
    var apiService: APIServiceType!
    
    // MARK: - Controller handlers
    
    var onSuccessSignIn: ((UserModelType) -> Void)?
    var onSignUpAction: (() -> Void)?
    
    // MARK: - API
    
    private func validateAndLogin() {
        guard let userName = userNameTextField.text, !userName.isEmpty,
            let password = passwordTextField.text, !password.isEmpty else {
                showAlertWith(title: "Invalid input", message: "Username or password is incorrent")
                return
        }
        
        apiService.loginWith(userName, password: password) { [weak self] error, userModel in
            if let error = error {
                self?.showAlertWith(title: "Failure", message: error.localizedDescription)
            } else if let userModel = userModel {
                self?.onSuccessSignIn?(userModel)
            } else {
                self?.showAlertWith(title: "Failure", message: "Unknown error")
            }
        }
    }
    
    // MARK: - Actions
    
    @IBAction private func loginAction(_ sender: UIButton) {
        validateAndLogin()
    }
    
    @IBAction private func signUpAction(_ sender: UIButton) {
        onSignUpAction?()
    }
}

extension SignInViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField.tag == userNameTextField.tag) {
            passwordTextField.becomeFirstResponder()
        } else if (textField.tag == passwordTextField.tag) {
            view.endEditing(true)
        }
        
        return false
    }
}
