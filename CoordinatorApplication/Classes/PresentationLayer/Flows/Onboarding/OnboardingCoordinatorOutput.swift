//
//  OnboardingCoordinatorOutput.swift
//  CoordinatorApplication
//
//  Created by Nikita Koltsov on 3/27/18.
//  Copyright © 2018 vironIT. All rights reserved.
//

protocol OnboardingCoordinatorOutput: class {
    var finishFlow: (() -> Void)? { get set }
}
