//
//  OnboardingCoordinator.swift
//  CoordinatorApplication
//
//  Created by Nikita Koltsov on 3/27/18.
//  Copyright © 2018 vironIT. All rights reserved.
//

import Foundation

final class OnboardingCoordinator: BaseCoordinator, OnboardingCoordinatorOutput {
    
    // MARK: - Properties
    
    private let router: RouterType
    private let onboardingFactory: OnboardingModuleFactoryType
    var finishFlow: (() -> Void)?
    
    // MARK: - Lifecycle
    
    init(router: RouterType, onboardingFactory: OnboardingModuleFactoryType) {
        self.router = router
        self.onboardingFactory = onboardingFactory
    }
    
    // MARK: - BaseCoordinator
    
    override func start() {
        let module = onboardingFactory.makeOnboardingModule()
        module.onFinishOnboarding = { [weak self] in
            self?.finishFlow?()
        }
        router.setRoot(module.toPresent, hideBar: true)
    }
}
