//
//  OnboardingViewController.swift
//  CoordinatorApplication
//
//  Created by Nikita Koltsov on 3/27/18.
//  Copyright © 2018 vironIT. All rights reserved.
//

import UIKit

class OnboardingViewController: UIViewController, OnboardingViewType {

    // MARK: - Properties
    
    var onFinishOnboarding: (() -> Void)?
    
    // MARK: - Actions

    @IBAction func finishAction(_ sender: UIButton) {
        onFinishOnboarding?()
    }
    
}
