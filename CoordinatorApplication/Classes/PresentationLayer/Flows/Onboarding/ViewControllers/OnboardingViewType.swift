//
//  OnboardingViewType.swift
//  CoordinatorApplication
//
//  Created by Nikita Koltsov on 3/27/18.
//  Copyright © 2018 vironIT. All rights reserved.
//

protocol OnboardingViewType: Presentable {
    var onFinishOnboarding: (() -> Void)? { get set }
}
