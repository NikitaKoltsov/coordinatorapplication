//
//  Router.swift
//  CoordinatorApplication
//
//  Created by Nikita Koltsov on 3/26/18.
//  Copyright © 2018 vironIT. All rights reserved.
//

import UIKit

final class Router {
    private weak var rootViewController: UINavigationController?
    
    init(navigationController: UINavigationController) {
        rootViewController = navigationController
    }
}

extension Router: RouterType {
    func present(_ module: Presentable?, animated: Bool) {
        guard let vc = module?.toPresent else { return }
        rootViewController?.present(vc, animated: animated, completion: nil)
    }
    
    func dismissModule(animated: Bool, completion: (() -> Void)?) {
        rootViewController?.dismiss(animated: animated, completion: completion)
    }
    
    func push(_ module: Presentable?, animated: Bool) {
        guard let vc = module?.toPresent else { return }
        rootViewController?.pushViewController(vc, animated: animated)
    }
    
    func popModule(animated: Bool) {
        rootViewController?.popViewController(animated: animated)
    }
    
    func popToRootModule(animated: Bool) {
        rootViewController?.popToRootViewController(animated: animated)
    }
    
    func setRoot(_ module: Presentable?, hideBar: Bool) {
        guard let vc = module?.toPresent else { return }
        rootViewController?.setViewControllers([vc], animated: false)
        rootViewController?.isNavigationBarHidden = hideBar
    }
}
