//
//  RouterType.swift
//  CoordinatorApplication
//
//  Created by Nikita Koltsov on 3/26/18.
//  Copyright © 2018 vironIT. All rights reserved.
//

import UIKit

protocol RouterType {
    func present(_ module: Presentable?)
    func present(_ module: Presentable?, animated: Bool)
    
    func dismissModule()
    func dismissModule(animated: Bool, completion: (() -> Void)?)
    
    func push(_ module: Presentable?)
    func push(_ module: Presentable?, animated: Bool)
    
    func popModule()
    func popModule(animated: Bool)
    func popToRootModule(animated: Bool)
    
    func setRoot(_ module: Presentable?)
    func setRoot(_ module: Presentable?, hideBar: Bool)
}

extension RouterType {
    func present(_ module: Presentable?) {
        present(module, animated: true)
    }
    
    func dismissModule() {
        dismissModule(animated: true, completion: nil)
    }
    
    func push(_ module: Presentable?) {
        push(module, animated: true)
    }
    
    func popModule() {
        popModule(animated: true)
    }
    
    func setRoot(_ module: Presentable?) {
        setRoot(module, hideBar: false)
    }
}
