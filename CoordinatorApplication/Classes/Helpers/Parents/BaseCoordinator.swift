//
//  BaseCoordinator.swift
//  CoordinatorApplication
//
//  Created by Nikita Koltsov on 3/20/18.
//  Copyright © 2018 vironIT. All rights reserved.
//

class BaseCoordinator: Coordinator {
    var childCoordinators: [Coordinator] = []
    
    func start() { }
}
