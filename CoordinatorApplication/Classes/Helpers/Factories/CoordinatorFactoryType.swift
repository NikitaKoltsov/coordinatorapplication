//
//  CoordinatorFactoryType.swift
//  CoordinatorApplication
//
//  Created by Nikita Koltsov on 3/26/18.
//  Copyright © 2018 vironIT. All rights reserved.
//

protocol CoordinatorFactoryType {
    
    func makeLoginCoordinator(router: RouterType, dataService: DataService) -> Coordinator & LoginCoordinatorOutput
    
    func makeOnboardingCoordinator(router: RouterType) -> Coordinator & OnboardingCoordinatorOutput
    
}
