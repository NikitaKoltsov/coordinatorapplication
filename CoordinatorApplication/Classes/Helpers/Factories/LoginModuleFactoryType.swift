//
//  LoginModuleFactory.swift
//  CoordinatorApplication
//
//  Created by Nikita Koltsov on 3/26/18.
//  Copyright © 2018 vironIT. All rights reserved.
//

protocol LoginModuleFactoryType {
    func makeSignInModule(apiService: APIServiceType) -> SignInViewType
    func makeSignUpModule(apiService: APIServiceType) -> SignUpViewType
}

extension LoginModuleFactoryType {
    func makeSignInModule(apiService: APIServiceType) -> SignInViewType {
        let vc = SignInViewController.viewControllerFrom(.login)
        vc.apiService = apiService
        return vc
    }
    
    func makeSignUpModule(apiService: APIServiceType) -> SignUpViewType {
        let vc = SignUpViewController.viewControllerFrom(.login)
        vc.apiService = apiService
        return vc
    }
}
