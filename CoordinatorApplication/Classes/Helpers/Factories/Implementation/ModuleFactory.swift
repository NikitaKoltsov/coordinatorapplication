//
//  ModuleFactory.swift
//  CoordinatorApplication
//
//  Created by Nikita Koltsov on 3/26/18.
//  Copyright © 2018 vironIT. All rights reserved.
//

final class ModuleFactory {
}

extension ModuleFactory: OnboardingModuleFactoryType { }

extension ModuleFactory: LoginModuleFactoryType { }
