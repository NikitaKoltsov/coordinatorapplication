//
//  CoordinatorFactory.swift
//  CoordinatorApplication
//
//  Created by Nikita Koltsov on 3/26/18.
//  Copyright © 2018 vironIT. All rights reserved.
//

final class CoordinatorFactory: CoordinatorFactoryType {
    
    func makeLoginCoordinator(router: RouterType, dataService: DataService) -> Coordinator & LoginCoordinatorOutput {
        return LoginCoordinator(router: router,
                                dataService: dataService,
                                loginFactory: ModuleFactory())
    }

    func makeOnboardingCoordinator(router: RouterType) -> Coordinator & OnboardingCoordinatorOutput {
        return OnboardingCoordinator(router: router,
                                     onboardingFactory: ModuleFactory())
    }
    
}
