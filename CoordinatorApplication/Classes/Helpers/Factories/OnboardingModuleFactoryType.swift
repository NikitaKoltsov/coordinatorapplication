//
//  OnboardingModuleFactory.swift
//  CoordinatorApplication
//
//  Created by Nikita Koltsov on 3/27/18.
//  Copyright © 2018 vironIT. All rights reserved.
//

protocol OnboardingModuleFactoryType {
    func makeOnboardingModule() -> OnboardingViewType
}

extension OnboardingModuleFactoryType {
    func makeOnboardingModule() -> OnboardingViewType {
        return OnboardingViewController.viewControllerFrom(.onboarding)
    }
}
