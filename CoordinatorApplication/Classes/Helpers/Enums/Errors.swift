//
//  Errors.swift
//  CoordinatorApplication
//
//  Created by Nikita Koltsov on 3/21/18.
//  Copyright © 2018 vironIT. All rights reserved.
//

enum APIError: Error {
    case existingAccount(String)
    case wrongCredentials(String)
    case internalServerError
    
    var localizedDescription: String {
        switch self {
        case .existingAccount(let description):
            return description
        case .wrongCredentials(let description):
            return description
        case .internalServerError:
            return "Internal server error"
        }
    }
}
