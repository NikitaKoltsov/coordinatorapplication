//
//  Constants.swift
//  CoordinatorApplication
//
//  Created by Nikita Koltsov on 3/22/18.
//  Copyright © 2018 vironIT. All rights reserved.
//

enum UserDefaultsConstants: String {
    case userName
    case onboardingWasShown
}
