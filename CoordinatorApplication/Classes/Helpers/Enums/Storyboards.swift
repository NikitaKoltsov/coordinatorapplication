//
//  Storyboards.swift
//  CoordinatorApplication
//
//  Created by Nikita Koltsov on 3/15/18.
//  Copyright © 2018 vironIT. All rights reserved.
//

enum Storyboards: String {
    case main = "Main"
    case login = "Login"
    case onboarding = "Onboarding"
}
