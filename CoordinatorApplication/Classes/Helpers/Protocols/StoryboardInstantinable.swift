//
//  StoryboardInstantinable.swift
//  CoordinatorApplication
//
//  Created by Nikita Koltsov on 3/15/18.
//  Copyright © 2018 vironIT. All rights reserved.
//

import UIKit

protocol StoryboardInstantiable: NSObjectProtocol {
    associatedtype ViewControllerType
    static func viewControllerFrom(_ storyboard: Storyboards) -> ViewControllerType
}

extension StoryboardInstantiable where Self: UIViewController {
    static func viewControllerFrom(_ storyboard: Storyboards) -> Self {
        return self.viewControllerIn(UIStoryboard(name: storyboard.rawValue, bundle: nil),
                                     withIdentifier: Self.typeName)
    }
    
    private static func viewControllerIn(_ storyboard: UIStoryboard, withIdentifier identifier: String) -> Self {
        return storyboard.instantiateViewController(withIdentifier: identifier) as! Self
    }
}

extension UIViewController: StoryboardInstantiable {}
