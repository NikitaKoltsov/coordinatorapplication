//
//  ClassName.swift
//  CoordinatorApplication
//
//  Created by Nikita Koltsov on 3/15/18.
//  Copyright © 2018 vironIT. All rights reserved.
//

import Foundation

protocol TypeName {
    static var typeName: String { get }
}

extension TypeName {
    static var typeName: String {
        return String(describing: Self.self)
    }
}

extension NSObject: TypeName {}
