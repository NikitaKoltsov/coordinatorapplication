//
//  Presentable.swift
//  CoordinatorApplication
//
//  Created by Nikita Koltsov on 3/21/18.
//  Copyright © 2018 vironIT. All rights reserved.
//

import UIKit

protocol Presentable: class {
    var toPresent: UIViewController { get }
}

extension UIViewController: Presentable {
    var toPresent: UIViewController {
        return self
    }
}
