//
//  AppCoordinator+LaunchInstructor.swift
//  CoordinatorApplication
//
//  Created by Nikita Koltsov on 3/26/18.
//  Copyright © 2018 vironIT. All rights reserved.
//

import Foundation

extension AppCoordinator {
    struct LaunchInstructor {
        enum ShowAction {
            case main, login, onboarding
            
            static func configure(onboardingWasShown: Bool, userIsSignedIn: Bool) -> ShowAction {
                switch (onboardingWasShown, userIsSignedIn) {
                case (true, false),
                     (false, false):
                    return .login
                case (true, true):
                    return .main
                case (false, true):
                    return .onboarding
                }
            }
        }
        
        // MARK: - Properties
        
        private var isOnboardingWasShown: Bool
        var isUserSignedIn: Bool
        
        // MARK: - Getters/setters
        
        var showAction: ShowAction {
            return ShowAction.configure(onboardingWasShown: isOnboardingWasShown, userIsSignedIn: isUserSignedIn)
        }
        
        // MARK: - Methods
        
        init(isUserSignedIn: Bool) {
            self.isUserSignedIn = isUserSignedIn
            isOnboardingWasShown = UserDefaults.standard.bool(forKey: UserDefaultsConstants.onboardingWasShown.rawValue)
        }
        
        func onboardingWasShown() {
            UserDefaults.standard.set(true, forKey: UserDefaultsConstants.onboardingWasShown.rawValue)
        }
    }
}
