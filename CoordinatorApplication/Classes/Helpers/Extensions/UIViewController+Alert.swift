//
//  UIViewController+Storyboard.swift
//  CoordinatorApplication
//
//  Created by Nikita Koltsov on 3/15/18.
//  Copyright © 2018 vironIT. All rights reserved.
//

import UIKit

extension UIViewController {
    func showAlertWith(title: String, message: String? = nil, completion:(() -> Void)? = nil) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alertController.addAction(okAction)
        
        present(alertController, animated: true, completion: nil)
    }
}

