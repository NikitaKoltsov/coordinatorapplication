//
//  LoginServiceType.swift
//  CoordinatorApplication
//
//  Created by Nikita Koltsov on 3/20/18.
//  Copyright © 2018 vironIT. All rights reserved.
//

import Foundation

protocol LoginServiceType: class {
    var loggedUserName: String? { get }
    
    func login(_ user: UserModelType)
    
    func logout()
    
    func isLoggedIn(_ user: UserModelType) -> Bool
}

final class LoginService {
    private(set) var loggedUserName: String?
    
    init() {
        loggedUserName = UserDefaults.standard.value(forKey: UserDefaultsConstants.userName.rawValue) as? String
    }
}

extension LoginService: LoginServiceType {
    func isLoggedIn(_ user: UserModelType) -> Bool {
        if let loggedUserName = loggedUserName {
            return user.userName == loggedUserName
        } else {
            return false
        }
    }
    
    func login(_ user: UserModelType) {
        UserDefaults.standard.set(user.userName, forKey: UserDefaultsConstants.userName.rawValue)
        loggedUserName = user.userName
    }
    
    func logout() {
        UserDefaults.standard.removeObject(forKey: UserDefaultsConstants.userName.rawValue)
        loggedUserName = nil
    }
}
