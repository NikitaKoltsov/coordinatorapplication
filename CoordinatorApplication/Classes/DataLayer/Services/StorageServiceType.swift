//
//  DataManagerType.swift
//  CoordinatorApplication
//
//  Created by Nikita Koltsov on 3/16/18.
//  Copyright © 2018 vironIT. All rights reserved.
//

import RealmSwift

protocol StorageServiceType: class {
    
    // UserModel
    func userModelWith(_ userName: String, password: String) -> UserModelType?
    
    func createUserModelWith(_ userName: String, password: String, email: String?) -> UserModelType?
}

final class RealmStorageService {
    
    // MARK: - Properties
    
    private var realm: Realm? {
        return try? Realm()
    }
    
    //MARK: - Convenience
    
    private static func setRealmConfiguration() {
        let configuration = Realm.Configuration(
            schemaVersion: 1,
            migrationBlock: { migration, oldSchemaVersion in
                if (oldSchemaVersion < 1) {
                    
                }
        })
        
        Realm.Configuration.defaultConfiguration = configuration
    }
}

extension RealmStorageService: StorageServiceType {
    
    func createUserModelWith(_ userName: String, password: String, email: String?) -> UserModelType? {
        let userModel = UserModel()
        userModel.userName = userName
        userModel.password = password
        
        try! realm?.write {
            realm?.add(userModel)
        }
        
        return userModel
    }
    
    func userModelWith(_ userName: String, password: String) -> UserModelType? {
        return realm?.objects(UserModel.self).filter("userName = \(userName) AND password = \(password)").first
    }
    
    func userModelWith(_ userName: String) -> UserModelType? {
        return realm?.object(ofType: UserModel.self, forPrimaryKey: userName)
    }
}
