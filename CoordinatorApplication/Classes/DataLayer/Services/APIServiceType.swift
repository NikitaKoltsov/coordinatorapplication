//
//  APIServiceType.swift
//  CoordinatorApplication
//
//  Created by Nikita Koltsov on 3/21/18.
//  Copyright © 2018 vironIT. All rights reserved.
//

protocol APIServiceType {
    func loginWith(_ userName: String, password: String, completion: @escaping((Error?, UserModelType?) -> Void))
    func registerWith(_ userName: String, password: String, email: String?, completion: @escaping((Error?, UserModelType?) -> Void))
}

extension RealmStorageService: APIServiceType {
    func loginWith(_ userName: String, password: String, completion: @escaping ((Error?, UserModelType?) -> Void)) {
        if let userModel = userModelWith(userName, password: password) {
            completion(nil, userModel)
        } else {
            completion(APIError.wrongCredentials("Invalid credentials!"), nil)
        }
    }
    
    func registerWith(_ userName: String, password: String, email: String?, completion: @escaping ((Error?, UserModelType?) -> Void)) {
        if userModelWith(userName) != nil {
            completion(APIError.existingAccount("Account with such user name exists"), nil)
        } else {
            if let userModel = createUserModelWith(userName, password: password, email: email) {
                completion(nil, userModel)
            } else {
                completion(APIError.internalServerError, nil)
            }
        }
    }
}
