//
//  DataService.swift
//  CoordinatorApplication
//
//  Created by Nikita Koltsov on 3/26/18.
//  Copyright © 2018 vironIT. All rights reserved.
//

import Foundation

struct DataService {
    let storageService: StorageServiceType
    let apiService: APIServiceType
    let loginService: LoginServiceType
}
