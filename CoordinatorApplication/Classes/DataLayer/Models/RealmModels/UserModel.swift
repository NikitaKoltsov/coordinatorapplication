//
//  UserModel.swift
//  CoordinatorApplication
//
//  Created by Nikita Koltsov on 3/16/18.
//  Copyright © 2018 vironIT. All rights reserved.
//

import RealmSwift

class UserModel: Object, UserModelType {
    @objc dynamic var userName: String = ""
    @objc dynamic var password: String = ""
    @objc dynamic var email: String? = nil
    
    override static func indexedProperties() -> [String] {
        return ["password"]
    }
    
    override static func primaryKey() -> String? {
        return "userName"
    }
}

