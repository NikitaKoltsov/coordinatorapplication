//
//  UserModelType.swift
//  CoordinatorApplication
//
//  Created by Nikita Koltsov on 3/21/18.
//  Copyright © 2018 vironIT. All rights reserved.
//

protocol UserModelType {
    
    var userName: String { get set }
    var password: String { get set }
    var email: String? { get set }
}

