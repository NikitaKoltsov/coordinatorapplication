//
//  AppCoordinator.swift
//  CoordinatorApplication
//
//  Created by Nikita Koltsov on 3/16/18.
//  Copyright © 2018 vironIT. All rights reserved.
//

import UIKit

final class AppCoordinator: BaseCoordinator {
    
    // MARK: - Properties
    
    private let router: RouterType
    private let coordinatorFactory: CoordinatorFactoryType
    private let dataService: DataService
    private lazy var launchInstructor = LaunchInstructor(isUserSignedIn: userIsSignedIn)
    
    // MARK: - Computed properties
    
    var userIsSignedIn: Bool {
        return dataService.loginService.loggedUserName != nil
    }
    
    // MARK: - Lifecycle
    
    init(router: RouterType, coordinatorFactory: CoordinatorFactoryType) {
        self.router = router
        self.coordinatorFactory = coordinatorFactory

        let realmService = RealmStorageService()
        dataService = DataService(storageService: realmService, apiService: realmService, loginService: LoginService())
    }
    
    // MARK: - BaseCoordinator
    
    override func start() {
        switch launchInstructor.showAction {
        case .login:
            runLoginFlow()
            break
        case .main:
            runMainFlow()
            break
        case .onboarding:
            runOnboardingFlow()
            break
        }
    }
    
    // MARK: - Flows
    
    private func runLoginFlow() {
        let coordinator = coordinatorFactory.makeLoginCoordinator(router: router, dataService: dataService)
        coordinator.finishFlow = { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.launchInstructor.isUserSignedIn = strongSelf.userIsSignedIn
            strongSelf.removeDependency(coordinator)
            strongSelf.start()
        }
        addDependency(coordinator)
        coordinator.start()
    }
    
    private func runMainFlow() {
        
    }
    
    private func runOnboardingFlow() {
        let coordinator = coordinatorFactory.makeOnboardingCoordinator(router: router)
        coordinator.finishFlow = { [weak self] in
            self?.removeDependency(coordinator)
            self?.start()
        }
        addDependency(coordinator)
        coordinator.start()
    }
}

