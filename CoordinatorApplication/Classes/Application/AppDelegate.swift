//
//  AppDelegate.swift
//  CoordinatorApplication
//
//  Created by Nikita Koltsov on 3/14/18.
//  Copyright © 2018 vironIT. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    private lazy var appCoordinator: Coordinator = {
        let navVC = UINavigationController()
        window?.rootViewController = navVC
        return AppCoordinator(router: Router(navigationController: navVC), coordinatorFactory: CoordinatorFactory())
    }()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        
        appCoordinator.start()
        window?.makeKeyAndVisible()
        
        return true
    }

}

